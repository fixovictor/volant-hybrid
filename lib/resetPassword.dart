import 'package:flutter/material.dart';
import 'main.dart' as LoginScreen;

// Reset password Widget
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // theme of the page
      theme: ThemeData(
        primaryColor: const Color(0xff8F0236), //ff033e
        primaryColorDark: const Color(0xff112B30),
        accentColor: const Color(0xff8F0236),
        cursorColor: const Color(0xff8F0236),
      ),

      // Creating routes
      routes: {
        'login': (context) => LoginScreen.MyApp(),
      },
      // Container with background image
      home: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("images/background.png"), fit: BoxFit.cover)),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: MyCustomForm(),
        ),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  // TextField controllers
  static TextEditingController _passwordFieldController;
  static TextEditingController _confirmationPasswordFieldController;
  static TextEditingController _emailFieldController;
  static TextEditingController _phoneFieldController;
  static TextEditingController _firstNameFieldController;
  static TextEditingController _secondNameFieldController;
  static BuildContext _globalContext;

  void initState() {
    super.initState();
    // Initializing TextField Controllers
    _passwordFieldController = TextEditingController();
    _confirmationPasswordFieldController = TextEditingController();
    _emailFieldController = TextEditingController();
    _phoneFieldController = TextEditingController();
    _firstNameFieldController = TextEditingController();
    _secondNameFieldController = TextEditingController();
  }

  void dispose() {
    // Destroying TextField Controllers
    _passwordFieldController.dispose();
    _confirmationPasswordFieldController.dispose();
    _emailFieldController.dispose();
    _phoneFieldController.dispose();
    _firstNameFieldController.dispose();
    _secondNameFieldController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _globalContext = context;
    // Build a Form widget using the _formKey created above.
    return ListView(
      children: <Widget>[
        Form(
          //Initialize the form key
          key: _formKey,
          child: Container(
            margin: const EdgeInsets.fromLTRB(30.0, 50.0, 30.0, 0.0),

            //Column with labels and text fields
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Reset password text widget variable
                _resetLabel,
                // Using sizedBox to create space between the two widgets
                SizedBox(height: 10.0),
                // Email textField
                _emailTextField,
                SizedBox(height: 10.0),
                // Button
                _resetButton,
                SizedBox(height: 10.0),
                // Button.
              ],
            ),
          ),
        ),
      ],
    );
  }

  // Text widget with instructions on how to reset password
  var _resetLabel = Text(
    'Use the email address you used to register your account to reset the password ',
    // Styling the text widget
    style: TextStyle(
      color: Colors.white,
      fontSize: 20,
    ),
  );

// Container email text filed
  var _emailTextField = Container(
    // Specify height of the container
    height: 45.0,
    // Email text filed
    child: TextField(
      // Specify the type of keyboard for input
      keyboardType: TextInputType.emailAddress,
      // Specify the text controller
      controller: _emailFieldController,
      // Decorating the widget
      decoration: InputDecoration(
        labelText: 'Enter email',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
        filled: true,
        fillColor: Colors.white70,
      ),
    ),
  );

  // Container with reset button
  var _resetButton = Container(
      // Specify width of the container
      width: 300.0,
      // Creating padding inside the container
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      // ButtonTheme widget with circular borders
      child: ButtonTheme(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: RaisedButton(
          textColor: Colors.white,
          elevation: 10.0,
          color: const Color(0xff8F0236),
          onPressed: () {
            // Going back to Login screen
            Navigator.pushNamed(_globalContext, 'login');
          },
          child: Text('Reset'),
        ),
      ));
}
