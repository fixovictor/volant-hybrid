import 'package:flutter/material.dart';
import 'main.dart' as LoginScreen;

// Widget with registration details
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Theme of the paage
      theme: ThemeData(
        primaryColor: const Color(0xff8F0236), //ff033e
        primaryColorDark: const Color(0xff112B30),
        accentColor: const Color(0xff8F0236),
        cursorColor: const Color(0xff8F0236),
      ),

      // Creating routes
      routes: {
        'login': (context) => LoginScreen.MyApp(), 
      },
      home: Container(
        // Creating container with background image
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("images/background.png"), fit: BoxFit.cover)),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: MyCustomForm(),
        ),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  // TextField controllers
  static TextEditingController _passwordFieldController;
  static TextEditingController _confirmationPasswordFieldController;
  static TextEditingController _emailFieldController;
  static TextEditingController _phoneFieldController;
  static TextEditingController _firstNameFieldController;
  static TextEditingController _secondNameFieldController;
  static BuildContext _globalContext;

  void initState() {
    super.initState();
    // Initializing TextField Controllers
    _passwordFieldController = TextEditingController();
    _confirmationPasswordFieldController = TextEditingController();
    _emailFieldController = TextEditingController();
    _phoneFieldController = TextEditingController();
    _firstNameFieldController = TextEditingController();
    _secondNameFieldController = TextEditingController();
  }

  void dispose() {
    // Destroying TextField Controllers;
    _passwordFieldController.dispose();
    _confirmationPasswordFieldController.dispose();
    _emailFieldController.dispose();
    _phoneFieldController.dispose();
    _firstNameFieldController.dispose();
    _secondNameFieldController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Initializing global context
    _globalContext = context;
    // Build a Form widget using the _formKey created above.

    // Returning a list view widget that is scrollable when all form widgets
    // cannot be displayed on the visible screen area
    return ListView(
      children: <Widget>[
        Form(
          key: _formKey,
          child: Container(
            // margins of the container
            margin: const EdgeInsets.fromLTRB(30.0, 50.0, 30.0, 0.0),

            // Column with text and text fields
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Login text widget the login label
                _loginLabel,
                // Using sizedBox to create space between the two widgets
                SizedBox(height: 10.0),
                // Phone number text field
                _phoneNumberTextField,
                 SizedBox(height: 10.0),
                // Email textField
                _emailTextField, 
                SizedBox(height: 10.0),
                 // first name textField
                _firstNameTextField,
                SizedBox(height: 10.0),
                 // Last name textField
                _secondNameTextField,
                SizedBox(height: 10.0),
                 // Password textField
                _passwordEditText,
                SizedBox(height: 10.0),
                //confirmation password text field
                _confirmationPasswordEditText,
                SizedBox(height: 10.0), 
                // Registration button.
                _registerButton,
                SizedBox(height: 10.0), 
                // Button navigating back to login page.
                _loginButton,
              ],
            ),
          ),
        ),
      ],
    );
  }

  // Text widget with registration label
  var _loginLabel = Text(
    'Register to become our customer',
    style: TextStyle(
      color: Colors.white,
      fontSize: 20,
    ),
  );

  // Row with Login button label and button theme
  var _loginButton = Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      Text(
        'Already have an account?',
        style: TextStyle(color: Colors.white),
      ),
      Container(
          margin: const EdgeInsets.only(left: 10.0),
          child: ButtonTheme(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: RaisedButton(
              textColor: Colors.white,
              color: const Color(0xff8F0236),
              child: Text('Login'),
              onPressed: () {
                // Going back to Login screen
                Navigator.pushNamed(_globalContext, 'login');
              },
            ),
          ))
    ],
  );

  // Phone number text field decoration and logic
  var _phoneNumberTextField = Container(
      height: 45.0,
      // Phone number text field
      child: TextField(
          keyboardType: TextInputType.number, //keyboard to show numbers only
          // Passing the phone number text field controller
          controller: _phoneFieldController,
          // Decorating the text field
          decoration: InputDecoration(
            labelText: 'Enter phone number e.g  0715678956',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
            ),
            filled: true,
            fillColor: Colors.white70,
          )));

  // Container with email text filed and label decoration
  var _emailTextField = Container(
    height: 45.0,
    // Email text field
    child: TextField(
      // Specifying the type of keyboard to be used on the text field
      keyboardType: TextInputType.emailAddress,
      // Passing the phone number text field controller
      controller: _emailFieldController,
      // Decorating the text field
      decoration: InputDecoration(
        labelText: 'Enter email',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
        filled: true,
        fillColor: Colors.white70,
        // enabledBorder: OutlineInputBorder(
        //   borderRadius: BorderRadius.all(Radius.circular(12.0)),
        //   borderSide: BorderSide(color: Colors.black, width: 2),
        // ),
        // focusedBorder: OutlineInputBorder(
        //   borderRadius: BorderRadius.all(Radius.circular(10.0)),
        //   borderSide: BorderSide(color: Colors.red),
        // ),
      ),
    ),
  );

  // Container with first name textField and its decoration
  var _firstNameTextField = Container(
      height: 45.0,
      child: TextField(
        // Passing the phone number text field controller
          controller: _firstNameFieldController,
          // Decorating the text field
          decoration: InputDecoration(
            labelText: 'Enter first name',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
            ),
            filled: true,
            fillColor: Colors.white70,
          )));

  // Container withSecond name text field and its decoration
  var _secondNameTextField = Container(
      height: 45.0,
      child: TextField(
        // Passing the phone number text field controller
          controller: _secondNameFieldController,
          // Decorating the widget
          decoration: InputDecoration(
            labelText: 'Enter last name',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
            ),
            filled: true,
            fillColor: Colors.white70,
          )));

  // Container widget with registration button
  var _registerButton = Container(
      width: 300.0,
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: ButtonTheme(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: RaisedButton(
          textColor: Colors.white,
          elevation: 10.0,
          color: const Color(0xff8F0236),
          onPressed: () {
            // Validate returns true if the form is valid, or false
            // otherwise.
          },
          child: Text('Register'),
        ),
      ));

  // Container widget holding password field and its decoration
  var _passwordEditText = Container(
    // Specify height of the container
    height: 45.0,
    child: TextField(
      // Passing the phone number text field controller
      controller: _passwordFieldController,
      obscureText: true,
      // Decorating the widget
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white70,
        labelText: 'Enter password',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
      ),
    ),
  );

  // Container widget holding confirmation password field and its decoration
  var _confirmationPasswordEditText = Container(
    // Specify height of the container
    height: 45.0,
    child: TextField(
      // Passing the phone number text field controller
      controller: _confirmationPasswordFieldController,
      obscureText: true,
      // Decorating the widget
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white70,
        labelText: 'Re-enter the password',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
      ),
    ),
  );
}
