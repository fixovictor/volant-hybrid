import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:volant_hybrid/Examples/passingDataToNewScreen/toDo.dart';
import 'package:volant_hybrid/Examples/passingDataToNewScreen/secondScreen.dart';

void main(){
  runApp(
    MaterialApp(
      title: 'Passing data',
      home: TodosScreen(
        todos: List.generate(200, 
        (i) => ToDo(
          'Todo $i',
          'A description of what need to be done for Todo $i'
        )
        )
      ),
    )
  );
}

class TodosScreen extends StatelessWidget {
  final List<ToDo> todos;

  TodosScreen({Key key, @required this.todos})
  : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        title: Text('Todos'),
      ),
      body: ListView.builder(
        itemCount: todos.length,
        itemBuilder: (context, index){
          return ListTile(
            title: Text(todos[index].title),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => SecondScreen(todo: todos[index],))
                  );
            },
          );
        },
        
      ),
    );
  }
}