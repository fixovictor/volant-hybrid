import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:volant_hybrid/Examples/passingDataToNewScreen/toDo.dart';

class SecondScreen extends StatelessWidget {
  final ToDo todo;

  SecondScreen({Key key, @required this.todo})
  : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(todo.title),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Text(todo.description),
      ),
    );
  }
}