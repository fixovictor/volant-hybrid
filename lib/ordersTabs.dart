import 'package:flutter/material.dart';
import 'main.dart' as LoginScreen;

// Widget displayed in Orders tab
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Theme of this page
      theme: ThemeData(
        primaryColor: const Color(0xff8F0236), //ff033e
        primaryColorDark: const Color(0xff112B30),
        accentColor: const Color(0xff8F0236),
        cursorColor: const Color(0xff8F0236),
      ),

      // Creating route
      routes: {
        'login': (context) => LoginScreen.MyApp(),
      },

      // Tab controller widget that handles navigation from one tab to another
      home: DefaultTabController(
        // Creating three tabs
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                // Tab names
                Tab(
                  child: Text('METRO'),
                ),
                Tab(
                  child: Text('CARGO & FREIGHT'),
                ),
                Tab(
                  child: Text('PACKAGING & MOVES'),
                ),
              ],
            ),
          ),
          body: OrdersTabView(),
        ),
      ),
    );
  }
}

// Orders view stateful widget
class OrdersTabView extends StatefulWidget {
  @override
  _OrdersTabViewState createState() => _OrdersTabViewState();
}

class _OrdersTabViewState extends State<OrdersTabView> {
  @override
  Widget build(BuildContext context) {
    return TabBarView(
      // Initializing the three tabs

      children: <Widget>[
        _metroTabView,
        _cargoTabView,
        _packagingTabView,
      ],
    );
  }

// Metro package orders tab view
  var _metroTabView = Card(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),
    ),
    margin: const EdgeInsets.fromLTRB(10, 10, 10, 310),
    elevation: 5,
    child: InkWell(
      splashColor: Colors.blue.withAlpha(30),
      onTap: () {
        // TO DO : Navigate to new class
      },
      child: Container(
        decoration: BoxDecoration(
            color: const Color(0xffFF9800),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          padding: EdgeInsets.only(top: 10),
          margin: const EdgeInsets.only(left: 5.0),
          child: Column(
            children: <Widget>[
              // Row having order no, order date and order status
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  // Column having order number label and value
                  Column(
                    children: <Widget>[
                      Text(
                        'Order No.',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      Text('kjskdjsj2'),
                    ],
                  ),

                  // Column having order date label and value
                  Column(
                    children: <Widget>[
                      Text(
                        'Order Date',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      Text('14-08-2020 05:00 pm'),
                    ],
                  ),

                  // Column having order status label and value
                  Column(
                    children: <Widget>[
                      Text(
                        'Status',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      Text('Received'),
                    ],
                  ),
                ],
              ),
              //sized box to create vertical space
              SizedBox(height: 10),
              // Divider
              const Divider(
                color: Colors.grey,
                height: 3,
                thickness: 1,
                indent: 5,
                endIndent: 5,
              ),
              //sized box to create vertical space
              SizedBox(height: 10),

              Row(
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      // Pick up icon image
                      Padding(
                        padding: const EdgeInsets.only(bottom: 65.0),
                        child: Image.asset('images/pickup_icon.png'),
                      ),

                      // Destination icon image
                      Padding(
                        padding: const EdgeInsets.only(bottom: 55.0),
                        child: Image.asset('images/destination_icon.png'),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Pick Up',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Using sizedBox to have text of fixed width
                      SizedBox(
                        width: 150,
                        // Place holder text widget
                        child: Text(
                            'Karen Country Club in Langata constiuency Karen Drive'),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Destination label.
                      Text(
                        'Destination',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      SizedBox(
                        // Creating fixed width for the text widget
                        width: 150,
                        // Place holder text widget
                        child: Text(
                            'Karen Country Club in Langata constiuency Karen Drive'),
                      ),
                    ],
                  ),
                  // SizedBox to create horizontal space
                  SizedBox(width: 40),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      // Sender label.
                      Text(
                        'Sender',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Using sizedBox to have text of fixed width
                      SizedBox(
                        width: 100,
                        // Place holder text widget
                        child: Text('Omollo Kamau Njuguna Omondi'),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Recipient label.
                      Text(
                        'Recipient',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      SizedBox(
                        width: 100,
                        // Place holder text widget
                        child: Text('Omollo Kamau Njuguna Omondi'),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ),
  );

  // Packaging and moves orders tab view
  var _packagingTabView = Card(

    // Creating rounded borders for the card
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),
    ),
    margin: const EdgeInsets.fromLTRB(10, 10, 10, 310),
    elevation: 5,
    child: InkWell(
      splashColor: Colors.blue.withAlpha(30),
      onTap: () {
        // TO DO : Navigate to new class
      },
      child: Container(
        decoration: BoxDecoration(
            color: const Color(0xffFF9800),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          padding: EdgeInsets.only(top: 10),
          margin: const EdgeInsets.only(left: 5.0),
          child: Column(
            children: <Widget>[
              // Row having order no, order date and order status
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  // Column having order number label and value
                  Column(
                    children: <Widget>[
                      Text(
                        'Order No.',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      // Place holder text widget
                      Text('kjskdjsj2'),
                    ],
                  ),

                  // Column having order date label and value
                  Column(
                    children: <Widget>[
                      Text(
                        'Order Date',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      // Place-holder text widget
                      Text('14-08-2020 05:00 pm'),
                    ],
                  ),

                  // Column having order status label and value
                  Column(
                    children: <Widget>[
                      Text(
                        'Status',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      // Place-holder text widget
                      Text('Received'),
                    ],
                  ),
                ],
              ),
              //sized box to create vertical space
              SizedBox(height: 10),
              // Divider
              const Divider(
                color: Colors.grey,
                height: 3,
                thickness: 1,
                indent: 5,
                endIndent: 5,
              ),
              //sized box to create vertical space
              SizedBox(height: 10),

              Row(
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      // Pick up icon image
                      Padding(
                        padding: const EdgeInsets.only(bottom: 65.0),
                        child: Image.asset('images/pickup_icon.png'),
                      ),

                      // Destination icon image
                      Padding(
                        padding: const EdgeInsets.only(bottom: 55.0),
                        child: Image.asset('images/destination_icon.png'),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Pick Up',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Using sizedBox to have text of fixed width
                      SizedBox(
                        width: 150,
                        // Place-holder text widget
                        child: Text(
                            'Karen Country Club in Langata constiuency Karen Drive'),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Destination label.
                      Text(
                        'Destination',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      SizedBox(
                        width: 150,
                        // Place-holder text widget
                        child: Text(
                            'Karen Country Club in Langata constiuency Karen Drive'),
                      ),
                    ],
                  ),
                  // SizedBox to create horizontal space
                  SizedBox(width: 40),
                  Column(
                    // Align column elements to the left side
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      // Sender label.
                      Text(
                        'Sender',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Using sizedBox to have text of fixed width
                      SizedBox(
                        width: 100,
                        // Place-holder text widget
                        child: Text('Omollo Kamau Njuguna Omondi'),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Recipient label.
                      Text(
                        'Recipient',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      SizedBox(
                        width: 100,
                        // Place-holder text widget
                        child: Text('Omollo Kamau Njuguna Omondi'),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ),
  );

// cargo and freight orders tab view
  var _cargoTabView = Card(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),
    ),
    margin: const EdgeInsets.fromLTRB(10, 10, 10, 310),
    elevation: 5,
    child: InkWell(
      splashColor: Colors.blue.withAlpha(30),
      onTap: () {
        // TO DO : Navigate to new class
      },
      
      child: Container(
        // decorating the shape of the container with box decoration widdget
        // with rounded borders
        decoration: BoxDecoration(
            color: const Color(0xffFF9800),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          padding: EdgeInsets.only(top: 10),
          margin: const EdgeInsets.only(left: 5.0),
          child: Column(
            children: <Widget>[
              // Row having order no, order date and order status
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  // Column having order number label and value
                  Column(
                    children: <Widget>[
                      Text(
                        'Order No.',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      // Place-holder text widget
                      Text('kjskdjsj2'),
                    ],
                  ),

                  // Column having order date label and value
                  Column(
                    children: <Widget>[
                      Text(
                        'Order Date',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      // Place-holder text widget
                      Text('14-08-2020 05:00 pm'),
                    ],
                  ),

                  // Column having order status label and value
                  Column(
                    children: <Widget>[
                      Text(
                        'Status',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      // Place-holder text widget
                      Text('Received'),
                    ],
                  ),
                ],
              ),
              //sized box to create vertical space
              SizedBox(height: 10),
              // Divider
              const Divider(
                color: Colors.grey,
                height: 3,
                thickness: 1,
                indent: 5,
                endIndent: 5,
              ),
              //sized box to create vertical space
              SizedBox(height: 10),

              Row(
                children: <Widget>[
                  // Column with icons
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      // Pick up icon image
                      Padding(
                        padding: const EdgeInsets.only(bottom: 65.0),
                        child: Image.asset('images/pickup_icon.png'),
                      ),

                      // Destination icon image
                      Padding(
                        padding: const EdgeInsets.only(bottom: 55.0),
                        child: Image.asset('images/destination_icon.png'),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Pick Up',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Using sizedBox to have text of fixed width
                      SizedBox(
                        width: 150,
                        // Place-holder text widget
                        child: Text(
                            'Karen Country Club in Langata constiuency Karen Drive'),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Destination label.
                      Text(
                        'Destination',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      SizedBox(
                        width: 150,
                        // Place-holder text widget
                        child: Text(
                            'Karen Country Club in Langata constiuency Karen Drive'),
                      ),
                    ],
                  ),
                  // SizedBox to create horizontal space
                  SizedBox(width: 40),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      // Sender label.
                      Text(
                        'Sender',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Using sizedBox to have text of fixed width
                      SizedBox(
                        // Restrict the width of the text widget
                        width: 100,
                        // Place-holder text widget
                        child: Text('Omollo Kamau Njuguna Omondi'),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      // Recipient label.
                      Text(
                        'Recipient',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      //sized box to create vertical space
                      SizedBox(height: 10),
                      SizedBox(
                        width: 100,
                        // Place-holder text widget
                        child: Text('Omollo Kamau Njuguna Omondi'),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
