import 'package:flutter/material.dart';
import 'main.dart' as LoginScreen;
import 'ordersTabs.dart' as OrdersClass;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  //Creating global context object
  static BuildContext _globalContext;

  @override
  Widget build(BuildContext context) {
    // Initializing global context
    _globalContext = context;

    return MaterialApp(
      theme: ThemeData(
        primaryColor: const Color(0xff8F0236), //ff033e
        primaryColorDark: const Color(0xff112B30),
        accentColor: const Color(0xff8F0236),
        cursorColor: const Color(0xff8F0236),
      ),

      // Creating routes.
      routes: {
        '/': (context) => MyAppBody(),
        '/login': (context) => LoginScreen.MyApp(),
        '/orders': (context) => OrdersClass.MyApp(),
      },
      initialRoute: '/',
    );
  }
}

class MyAppBody extends StatefulWidget {
  @override
  _MyAppBodyState createState() => _MyAppBodyState();
}

class _MyAppBodyState extends State<MyAppBody> {
  static BuildContext _globalContext;

  @override
  Widget build(BuildContext context) {
    _globalContext = context;

    return Scaffold(

      // Creating app bar of height 0.0
      appBar: AppBar(
        toolbarHeight: 0.0,
      ),

      // Creating Bottom app bar
      bottomNavigationBar: BottomAppBar(
        color: const Color(0xffF1F1F1),
        child: _bottomAppBar,
      ),
      body: Container(
        height: 250.0,
        child: Stack(
          children: <Widget>[
            Container(
              height: 180.0,
              color: const Color(0xff8F0236),
              padding: EdgeInsets.fromLTRB(5.0, 10.0, 0.0, 0.0),
              child: Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      // Text Widget with labels
                      Text(
                        'Hello,',
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        'Fixo Victor',
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        'A/C. Business',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),

                  // SizedBox to create horizontal space
                  SizedBox(
                    width: 20.0,
                  ),

                  // Column with Company Logo and Motto
                  Column(
                    children: <Widget>[
                      Image.asset(
                        'images/final_logo.png',
                        width: 150.0,
                      ),
                      Text(
                        'Fast, Secure & Efficient',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  )
                ],
              ),
            ),

            // Positioned Widget with floating Box Container
            Positioned(
              right: 20.0,
              left: 20.0,
              top: 155.0,
              child: Container(
                padding: EdgeInsets.only(top: 10.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                // color: Colors.white,
                height: 50.0,
                width: 400.0,
                child: _banner,
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Styling the bottom app bar in a variable
  var _bottomAppBar = Container(
    height: 325.0,

    // Column widget with images of packages
    child: Column(
      children: <Widget>[
        Container(
          height: 265.0,
          padding: EdgeInsets.only(
            top: 10.0,
          ),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset(
                    'images/final_metro.png',
                    width: 172,
                  ),
                  Image.asset(
                    'images/final_cargo.png',
                    width: 172,
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset(
                    'images/final_p_c.png',
                    width: 172,
                  ),
                  Image.asset(
                    'images/final_logistics.png',
                    width: 172,
                  ),
                ],
              ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {
                // Navigate to orders class
                Navigator.pushNamed(_globalContext, '/orders');
              },
              child: Column(
                children: <Widget>[
                  Icon(Icons.queue),
                  Text(
                    'My Orders',
                  ),
                ],
              ),
            ),
            Column(
              children: <Widget>[
                Icon(
                  Icons.home,
                ),
                Text(
                  'Home',
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Icon(
                  Icons.person,
                ),
                Text(
                  'More',
                ),
              ],
            ),
          ],
        ),
      ],
    ),
  );

  // Row widget with number of orders delivered, in transit and those flagged
  var _banner = Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Column(
        children: <Widget>[
          Text('2 Delivered'),
          Image.asset('images/yellow_circle.png')
        ],
      ),
      Column(
        children: <Widget>[
          Text('2 In Transit'),
          Image.asset('images/green_circle.png')
        ],
      ),
      Column(
        children: <Widget>[
          Text('2 Flagged'),
          Image.asset('images/red_circle.png')
        ],
      ),
    ],
  );
}
