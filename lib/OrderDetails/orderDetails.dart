import 'package:flutter/material.dart';
import '../main.dart' as LoginScreen;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: const Color(0xff8F0236), //ff033e
        primaryColorDark: const Color(0xff112B30),
        accentColor: const Color(0xff8F0236),
        cursorColor: const Color(0xff8F0236),
      ),
      routes: {
        'login': (context) => LoginScreen.MyApp(),
      },
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            // Bottom navigation bar
            bottom: TabBar(
              tabs: [
                // Tab header labels
                Tab(
                  child: Text('TRACKING'),
                ),
                Tab(
                  child: Text('ORDER DETAILS'),
                ),
                Tab(
                  child: Text('BILLING'),
                ),
              ],
            ),
          ),
          body: OrdersTabView(),
        ),
      ),
    );
  }
}

class OrdersTabView extends StatefulWidget {
  @override
  _OrdersTabViewState createState() => _OrdersTabViewState();
}

class _OrdersTabViewState extends State<OrdersTabView> {
  @override
  Widget build(BuildContext context) {
    return TabBarView(
      children: <Widget>[
        // Place-holder text
        Text('To be done later'),
        // Specifying tabs

        //tracking tab view
        _trackingTabView,
        // orders details tab view
        _orderDetailsTabView,
        //_packagingTabView,
        _billingTabView,
      ],
    );
  }

  // Horizontal diver widget
  static var _divider = const Divider(
    color: Colors.grey,
    height: 3,
    thickness: 1,
    indent: 5,
    endIndent: 5,
  );
  // SizedBox to create vertical space
  static var _verticalSpacer = const SizedBox(height: 10);

  // Container with order details widgets
  var _orderDetailsTabView = Container(
    // Decorating the container
    decoration: BoxDecoration(
      border:
          Border(left: BorderSide(color: const Color(0xffFF9800), width: 5)),
      borderRadius: BorderRadius.circular(10.0),
    ),
    child: Column(
      children: <Widget>[
        // Row having order no, order date and order status
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            // Column having order number label and value
            Column(
              children: <Widget>[
                Text(
                  'Order No.',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                // Place-holder text
                Text('kjskdjsj2'),
              ],
            ),

            // Column having order date label and value
            Column(
              children: <Widget>[
                Text(
                  'Order Date',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                // Place-holder text
                Text('14-08-2020 05:00 pm'),
              ],
            ),

            // Column having order status label and value
            Column(
              children: <Widget>[
                Text(
                  'Status',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                // Place-holder text
                Text('Received'),
              ],
            ),
          ],
        ),
        //sized box to create vertical space
        _verticalSpacer,
        // Divider
        _divider,
        //sized box to create vertical space
        _verticalSpacer,

        // Row with image widgets
        Row(
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.max,
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                // Pick up icon image
                Padding(
                  padding: const EdgeInsets.only(bottom: 65.0),
                  child: Image.asset('images/pickup_icon.png'),
                ),

                // Destination icon image
                Padding(
                  padding: const EdgeInsets.only(bottom: 55.0),
                  child: Image.asset('images/destination_icon.png'),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Pick Up',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                SizedBox(height: 10),
                // Using sizedBox to have text of fixed width
                SizedBox(
                  // Restrict width of the text widget
                  width: 150,
                  // Place-holder text
                  child: Text(
                      'Karen Country Club in Langata constiuency Karen Drive'),
                ),
                //sized box to create vertical space
                SizedBox(height: 10),
                // Destination label.
                Text(
                  'Destination',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                SizedBox(height: 10),
                SizedBox(
                  // Restrict width of the text
                  width: 150,
                  // Place-holder text
                  child: Text(
                      'Karen Country Club in Langata constiuency Karen Drive'),
                ),
              ],
            ),
            // SizedBox to create horizontal space
            SizedBox(width: 40),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                // Sender label.
                Text(
                  'Distance(Metres)',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                SizedBox(height: 10),
                // Using sizedBox to have text of fixed width
                SizedBox(
                  width: 100,
                  child: Text('45 Km'),
                ),
                //sized box to create vertical space
                SizedBox(height: 10),
                // Recipient label.
                Text(
                  'Transported By',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                SizedBox(height: 10),
                SizedBox(
                  width: 100,
                  // Place-holder text
                  child: Text('Express Bike'),
                ),
              ],
            ),
          ],
        ),

        _verticalSpacer,
        // Divider
        _divider,
        //sized box to create vertical space
        _verticalSpacer,

        // Row with sender label and name, recipient label and name
        // sender phone number label and value, recipient phone numbe and value
        Row(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Sender',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                SizedBox(height: 10),
                // Using sizedBox to have text of fixed width
                SizedBox(
                  // Restrict width of the text widget
                  width: 150,
                  // Place-holder text
                  child: Text('Gabriel Jesus Mane'),
                ),
                //sized box to create vertical space
                SizedBox(height: 10),
                // Destination label.
                Text(
                  'Recipient',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                SizedBox(height: 10),
                SizedBox(
                  // Restrict width of the text
                  width: 150,
                  // Place-holder text
                  child: Text('Didie Drogba Mikel'),
                ),
              ],
            ),
            // SizedBox to create horizontal space
            SizedBox(width: 40),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                // Sender label.
                Text(
                  'Phone Number',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                _verticalSpacer,
                // Using sizedBox to have text of fixed width
                SizedBox(
                  width: 100,
                  // Place-holder text
                  child: Text('+2541111111111'),
                ),
                //sized box to create vertical space
                _verticalSpacer,
                // Recipient label.
                Text(
                  'Phone Number',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                _verticalSpacer,
                SizedBox(
                  width: 100,
                  // Place-holder text
                  child: Text('+2541111111111'),
                ),
              ],
            ),
          ],
        ),

        // Divider
        _divider,
        //sized box to create vertical space
        _verticalSpacer,

        //Row with Method of paymnet and amount payed label and value
        Row(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Pay Method',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                _verticalSpacer,
                // Using sizedBox to have text of fixed width
                SizedBox(
                  width: 150,
                  // Place-holder text
                  child: Text('M-pesa'),
                ),
              ],
            ),
            // SizedBox to create horizontal space
            SizedBox(width: 40),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                // Amount label.
                Text(
                  'Amount',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                _verticalSpacer,
                // Using sizedBox to have text of fixed width
                SizedBox(
                  width: 100,
                  // // Place-holder text
                  child: Text(' ksh. 50,000'),
                ),
              ],
            ),
          ],
        ),

        // Divider
        _divider,
        //sized box to create vertical space
        _verticalSpacer,

        //Row with pick up date label and value
        Row(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Pick up date label
                Text(
                  'Pickup Date',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                _verticalSpacer,
                // Using sizedBox to have text of fixed width
                SizedBox(
                  width: 150,
                  // // Place-holder text
                  child: Text('13-08-2020 5:09 pm'),
                ),
              ],
            ),
          ],
        ),

         // Divider
        _divider,
        //sized box to create vertical space
        _verticalSpacer,

        // Row with description label and value
         Row(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Description label
                Text(
                  'Description',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                _verticalSpacer,
                // Using sizedBox to have text of fixed width
                SizedBox(
                  width: 150,
                  // Place-holder text
                  child: Text('Pickup the luggage from Easy coach parcels office Nairobi CBD and deliver them to Prosperity house 1st Floor, Westlands road'),
                ),
              ],
            ),
          ],
        ),
        // Divider
        _divider,
        //sized box to create vertical space
        _verticalSpacer,
        // Row with Instructions Label and value
        Row(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Instructions label
                Text(
                  'Instructions',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
                //sized box to create vertical space
                _verticalSpacer,
                // Using sizedBox to have text of fixed width
                SizedBox(
                  width: 150,
                  // // Place-holder text
                  child: Text('Pickup the luggage from Easy coach parcels office Nairobi CBD and deliver them to Prosperity house 1st Floor, Westlands road'),
                ),
              ],
            ),
          ],
        ),
      ],
    ),
  );

// cargo and freight orders tab view
 var _billingTabView = Text("Billing tab");
 var _trackingTabView = Text("Tracking");
}
