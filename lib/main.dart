import 'package:flutter/material.dart';
import 'register.dart' as RegistrationClass;
import 'resetPassword.dart' as ResetPasswordClass;
import 'home.dart' as HomeClass;
import 'Examples/httpGet.dart' as TestClass;

// Main page of the app
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Form Validation Demo';

    return MaterialApp(
      title: appTitle,

      // Themes of the main page
      theme: ThemeData(
        primaryColor: const Color(0xff8F0236), //ff033e
        primaryColorDark: const Color(0xff112B30),
        accentColor: const Color(0xff8F0236),
        cursorColor: const Color(0xff8F0236),
      ),

      // Creating routes
      initialRoute: '/',
      routes: {
        '/': (context) => MyCustomForm(),
        '/register': (context) => RegistrationClass.MyApp(),
        '/resetPassword': (context) => ResetPasswordClass.MyApp(),
        '/home': (context) => HomeClass.MyApp(),
        '/test': (context) => TestClass.MyApp(),
      },
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  // TextField controller
  TextEditingController _passwordFieldController;
  TextEditingController _emailFieldController;
  static BuildContext _globalContext;

  void initState() {
    super.initState();
    // Initializing TextFieldController
    _passwordFieldController = TextEditingController();
    _emailFieldController = TextEditingController();
  }

  void dispose() {
    // Destroying TextFieldController;
    _passwordFieldController.dispose();
    _emailFieldController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _globalContext = context;
    // Build a Form widget using the _formKey created above.
    return Container(
      // creating a container with background image
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("images/background.png"), fit: BoxFit.cover)),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: ListView(
          children: <Widget>[
            Form(
              // Initialize the form key
              key: _formKey,
              child: Container(
                margin: const EdgeInsets.fromLTRB(30.0, 200.0, 30.0, 0.0),
                // Column with login label, email address text field
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _loginLabel,
                    // Adding sizedBox to create space between the two widgets
                    // and password text field
                    SizedBox(height: 10.0),
                    // Email textField
                    Container(
                      height: 45.0,
                      // Email text field
                      child: TextField(
                        //Pass email text filed controller to this field
                        controller: _emailFieldController,
                        decoration: InputDecoration(
                          labelText: 'Enter email',
                          // Decorating the text field
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(12.0)),
                          ),
                          filled: true,
                          fillColor: Colors.white70,
                          // enabledBorder: OutlineInputBorder(
                          //   borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          //   borderSide: BorderSide(color: Colors.black, width: 2),
                          // ),
                          // focusedBorder: OutlineInputBorder(
                          //   borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          //   borderSide: BorderSide(color: Colors.red),
                          // ),
                        ),
                      ),
                    ),

                    // SizedBox to create vertical space
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      height: 45.0,
                      // Password text field
                      child: TextField(
                        // Pass password text filed controller to this field
                        controller: _passwordFieldController,
                        obscureText: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white70,
                          labelText: 'Enter password',
                          // Decorating the text field
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(12.0)),
                          ),
                        ),
                      ),
                    ),
                    Container(
                        width: 300.0,
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        // Creating rectangular border around the button
                        child: ButtonTheme(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: RaisedButton(
                            textColor: Colors.white,
                            elevation: 10.0,
                            color: const Color(0xff8F0236),
                            onPressed: () {
                              // When button is pressed, navigate to 'test' route
                              Navigator.pushNamed(context, '/home');
                            },
                            child: Text('Login'),
                          ),
                        )),
                    _signUpLink,
                    _forgotPasswordLink,
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Login label text field
  var _loginLabel = Text(
    'Login using email and password',
    style: TextStyle(
      color: Colors.white,
      fontSize: 20,
    ),
  );

  // Forgot password row with label text field
  var _forgotPasswordLink = Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: <Widget>[
      Text(
        'Forgot password?',
        style: TextStyle(color: Colors.white),
      ),
      Container(
        margin: const EdgeInsets.only(left: 10.0),
        child: ButtonTheme(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          child: RaisedButton(
            textColor: Colors.white,
            color: const Color(0xff8F0236),
            child: Text('Reset'),
            onPressed: () {
              // Navigate to 'resetPassword' route
              Navigator.pushNamed(_globalContext, '/resetPassword');
            },
          ),
        ),
      ),
    ],
  );

  // Row widget with  Text widget and sign up button
  var _signUpLink = Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      Text(
        'Have no account?',
        style: TextStyle(color: Colors.white),
      ),
      Container(
          margin: const EdgeInsets.only(left: 10.0),
          child: ButtonTheme(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: RaisedButton(
              textColor: Colors.white,
              color: const Color(0xff8F0236),
              child: Text('Sign Up'),
              onPressed: () {
                // Navigate to registration activity
                Navigator.pushNamed(_globalContext, '/register');
              },
            ),
          ))
    ],
  );
}
